package com.appinlab.todolist.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appinlab.todolist.R;
import com.appinlab.todolist.models.ListData;
import com.appinlab.todolist.models.Note;

import java.util.List;

/**
 * Created by MTN on 09/12/2017.
 */

public class TodoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Note> noteList;

    public TodoAdapter(List<Note> noteList) {
        this.noteList = noteList;
    }

    public void setNoteList(List<Note> noteList) {
        this.noteList = noteList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.note_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        Note note = noteList.get(position);

        viewHolder.titreTextView.setText(note.getTitre());
        viewHolder.detailsTextView.setText(note.getDetails());
    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView titreTextView;
        TextView detailsTextView;

        public ViewHolder(View itemView) {
            super(itemView);

            titreTextView = itemView.findViewById(R.id.titreTextView);
            detailsTextView = itemView.findViewById(R.id.detailsTextView);
        }
    }
}
