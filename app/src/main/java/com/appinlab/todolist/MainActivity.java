package com.appinlab.todolist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.appinlab.todolist.adapters.TodoAdapter;
import com.appinlab.todolist.models.ListData;
import com.appinlab.todolist.models.Note;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

public class MainActivity extends AppCompatActivity {

    List<Note> noteList;
    RecyclerView recyclerView;
    TodoAdapter todoAdapter;
    Button btnEnregistrer;

    EditText titreEditText;
    EditText detailsEditText;

    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        realm = Realm.getDefaultInstance();

        recyclerView = findViewById(R.id.recyclerView);
        titreEditText = findViewById(R.id.titreEditText);
        detailsEditText = findViewById(R.id.detailsEditText);
        btnEnregistrer = findViewById(R.id.btnEnregistrer);

        noteList = realm.where(Note.class).findAll();

        todoAdapter = new TodoAdapter(noteList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(todoAdapter);

        btnEnregistrer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveNote();
            }
        });
    }

    private void update(){
        noteList = realm.where(Note.class).findAll();
        todoAdapter.setNoteList(noteList);
    }

    private void saveNote(){
        String titre = titreEditText.getText().toString();
        String details = detailsEditText.getText().toString();

        if(titre.isEmpty() || details.isEmpty()){
            Toast.makeText(this, "Titre et description obligatoires", Toast.LENGTH_SHORT).show();
            return;
        }

        final Note note = new Note(titre, details);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(note);
                update();
                effacerFormulaire();
            }
        });
    }

    private void effacerFormulaire(){
        titreEditText.setText("");
        detailsEditText.setText("");

        titreEditText.requestFocus();
    }
}
