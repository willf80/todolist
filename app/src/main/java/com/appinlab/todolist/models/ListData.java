package com.appinlab.todolist.models;

/**
 * Created by MTN on 09/12/2017.
 */

public class ListData {
    private int type;
    private Object data;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
