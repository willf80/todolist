package com.appinlab.todolist.models;

/**
 * Created by MTN on 09/12/2017.
 */

public class Header {
    private String titre;

    public Header() {
    }

    public Header(String titre) {
        this.titre = titre;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }
}
