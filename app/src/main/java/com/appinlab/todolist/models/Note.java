package com.appinlab.todolist.models;

import io.realm.RealmObject;

/**
 * Created by MTN on 09/12/2017.
 */

public class Note extends RealmObject {
    private String titre;
    private String details;

    public Note() {}

    public Note(String titre, String details) {
        this.titre = titre;
        this.details = details;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
